const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const glob = require('glob');
const _ = require('lodash');
const notifier = require('node-notifier');


const debug = /--debug/.test(process.argv[2]);

if (process.mas) app.setName('Electron APIs');

let mainWindow = null;
let login = null;

function initialize() {
  makeSingleInstance();

  loadJS();

  function createWindow() {
    login = new BrowserWindow({
      width: 300,
      height: 250,
      title: 'Login',
      frame: false,
      webPreferences: {
        nodeIntegration: true
      }
    });
    login.loadFile('login.html');

    const windowOptions = {
      width: 800,
      height: 600,
      title: 'XBOT',
      show: false,
      webPreferences: {
        nodeIntegration: true,
        preload: path.join(__dirname, 'preload.js')
      }
    };

    switch (process.platform) {
      case 'darwin':
        windowOptions.icon = path.join(__dirname, '/assets/icons/app/logo.icns');
        break;
      case 'win32':
        windowOptions.icon = path.join(__dirname, '/assets/icons/app/logo.ico');
        break;
      default:
        windowOptions.icon = path.join(__dirname, '/assets/icons/app/logo.png');
    }

    mainWindow = new BrowserWindow(windowOptions);
    mainWindow.loadFile('index.html')

    if (debug) {
      mainWindow.webContents.openDevTools();
      mainWindow.maximize();
      require('devtron').install();
    }

    mainWindow.on('closed', () => {
      mainWindow = null;
    });

    login.on('close', () => {
      login = null;
    });
  }

  ipcMain.on('login', (e, { error, username }) => {
    if (error)
      notifier.notify({
        title: 'Login failed',
        message: error || 'Unkown reason'
      });
    else if (username) {
      mainWindow.show();
      login.hide();
    }
    e.returnValue = true;
  });

  ipcMain.once('quit', e => {
    app.quit();
  });

  app.on('ready', createWindow);

  app.on('window-all-closed', function () {
    if (process.platform !== 'darwin')
      app.quit();
  });

  app.on('activate', function () {
    if (mainWindow === null)
      createWindow();
  });
}

function makeSingleInstance() {
  if (process.mas)
    return;

  app.requestSingleInstanceLock();

  app.on('second-instance', () => {
    if (mainWindow) {
      if (mainWindow.isMinimized())
        mainWindow.restore();
      mainWindow.focus();
    }
  });
}

function loadJS() {
  const files = glob.sync(path.join(__dirname, 'main/**/*.js'));
  _.forEach(files, file => { require(file) });
}

initialize();