const $ = require('jquery');
const { ipcRenderer } = require('electron');


$('#btnLogin').on('click', () => {
  const username = $('#txtUsername').val();
  const password = $('#txtPassword').val();
  if (username && password)
    ipcRenderer.sendSync('login', { username });
  else
    ipcRenderer.sendSync('login', { error: 'Wrong Username or password' });
});

$('#btnCancel').on('click', () => {
  ipcRenderer.sendSync('quit');
})